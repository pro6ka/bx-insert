<?php
	$arComponentDescription = array(
		'NAME' => GetMessage('UTLAB_CONTACTS_NAME'),
		'DESCRIPTION' => GetMessage('UTLAB_CONTACTS_DESCRIPTION'),
		'ICON' => '/images/icon.gif',
		'PATH' => array(
			'ID' => 'UTLAB',
			'CHILD' => array(
				'ID' => 'utlab.contacts',
				'NAME' => 'Контакты для диллеров'
			),
		),
		'AREA_BUTTONS' => array(
			array(
				'URL' => 'javascript:alert("Это кнопка!!!");',
				'TITLE' => 'Это кнопка!',
			),
		),
		'CACHE_PATH' => 'Y',
		'COMPLEX' => 'N'
	);
