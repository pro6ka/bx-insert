# bitrix.insertions
## Использование
В тексте делаем вставку вида ##MY_FORM(123, test)## 
Здесь MY_FORM имя метода, который будет вызван для отработки контента (его нужно написать в /local/components/vendor_name/insertions/class.php), оно будет преобразовано в имя вида myForm($data, $template).
В качестве $data могут быть любые параметры, в любом количестве.
Для этого нужно сделать вызов ##MY_FORM({1, 2, 3}, template_name)##
...
## Вызов
```php
$APPLICATION->IncludeComponent('vendor_name:insertions', 'template_name', $data);
```
В `$data` помещаем контент который нужно обработать.

