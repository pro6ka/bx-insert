<?php

	/**
	 * Class Insert
	 * content format ##INSERT_NAME{{DATA=123;;MY_DATA=abc bcd cdf}}##
	 */
	class Insert extends CBitrixComponent {

		public function getResult()
		{
			$curContent = $this->arParams['CONTENT'];
			foreach ($this->getInserts() as $insert) {
				$this->setResult($insert);
				$content = $this->setTemplate($insert);
				$curContent = preg_replace(
					'~##' . $insert . '##~',
					$content,
					$curContent,
					1
				);
			}
			DHUtils::dd($curContent);
		}

		/**
		 * @return mixed
		 */
		private function getInserts()
		{
			preg_match_all('~##(.*[^#])##~', $this->arParams['CONTENT'], $m);
			return $m[1];
		}

		private function setResult($insert)
		{
			preg_match('~{{(.*[^}])}}~', $insert, $m);
			$this->arResult = array();
			foreach (explode(';;', $m[1]) as $data) {
				$param = explode('=', $data);
				$this->arResult[$param[0]] = trim($param[1]);
			}
		}

		private function setTemplate($insert)
		{
			preg_match('~^(\w+)~', $insert, $m);
			$templateName = strtolower(
				str_replace('_', '-', trim($m[1]))
			);
			$this->arResult['TEMPLATE'] = $templateName;
			$this->initComponent($this->getName(), $templateName);
			$this->setResult($insert);
			ob_start();
			$this->includeComponentTemplate();
			$result = ob_get_clean();
			return $result;
		}
	}