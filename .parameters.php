<?php
	$arComponentParameters = array(
		'GROUPS' => array(
			'RESOURCES' => array(
				'NAME' => GetMessage('UTLAB_CONTACTS_RESOURCES'),
				'SORT' => 200,
			),
		),
		'PARAMETERS' => array(
			'SITES_IBLOCK' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_SITES_IBLOCK'),
				'TYPE' => 'STRING',
				'DEFAULT' => '2179',
			),
			'DEALERS_IBLOCK' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_DEALERS_IBLOCK'),
				'TYPE' => 'STRING',
				'DEFAULT' => '2250',
			),
			'SITE_ID' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_SITE_ID'),
				'TYPE' => 'STRING',
				'DEFAULT' => SITE_ID,
			),
			'SITE_SETTINGS_FIELDS' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_SITE_SETTINGS_FIELDS'),
				'TYPE' => 'STRING',
				'DEFAULT' => array('ID', 'IBLOCK_ID'),
				'MULTIPLE' => 'Y',
			),
			'SITE_SETTINGS_PROPERTIES' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_SITE_SETTINGS_PROPERTIES'),
				'TYPE' => 'STRING',
				'DEFAULT'=> array(),
				'MULTIPLE' => 'Y',
			),
			'DEALERS_FIELDS' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_DEALERS_FIELDS'),
				'TYPE' => 'STRING',
				'DEFAULT' => array('ID', 'IBLOCK_ID'),
				'MULTIPLE' => 'Y',
			),
			'DEALERS_PROPERTIES' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_DEALERS_PROPERTIES'),
				'TYPE' => 'STRING',
				'DEFAULT'=> array(),
				'MULTIPLE' => 'Y',
			),
			'CALL_CENTER' => array(
				'PARENT' => 'RESOURCES',
				'NAME' => GetMessage('UTLAB_CONTACTS_CALL_CENTER')
			),
		)
	);
